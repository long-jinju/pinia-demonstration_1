import { defineStore } from 'pinia'
import { photoList } from '@/utils/photo'
export const asyncStore = defineStore({
  id: 'async',
  state: () => ({
    imgSrc: []
  }),
  actions: {
    /**
     * 设置图片地址
     * @param { string } data 
     */
    setImgSrc (data) {
      this.imgSrc = data
    },
    /**
     * 获取图片地址
     * @returns { string } 图片地址
     */
    async fetchImgSrc () {
      const list =  await photoList(28)
      this.setImgSrc(list)
    }
  }
})